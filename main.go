package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
)

// CONFIG
var (
	file    = flag.String("file", "", "Import data file")
	meta    = flag.Bool("meta", true, "write additional data to separate lines after password")
	dry     = flag.Bool("dry", false, "dry run, just cat everything out with hidden password")
	filters = map[string]string{} // will replace every occurance of <key> to <value> on every line

	force = flag.Bool("force", false, "Overwrite existing paswords") // Not working atm
	errs  []error
)

type item struct {
	title    string
	password string
	username string
	url      string
	notes    string
	sections []string
}

func main() {
	flag.Parse()

	if len(*file) == 0 {
		fmt.Println("You must specify file to import")
		os.Exit(1)
	}

	rc := make(chan []string)
	counter := 0
	errcounter := 0
	go readFile(rc)
	for record := range rc {
		item := item{record[0], record[1], record[2], record[3], record[4], record[5:]}
		counter++

		if *dry {
			insert(item, exec.Command("cat"))
		} else {
			var args []string
			args = append(args, "insert")
			if *force {
				args = append(args, "-f")
			}
			if *meta {
				args = append(args, "-m")
			}
			args = append(args, item.title)
			err := insert(item, exec.Command("pass", args...))
			if err != nil {
				errcounter++
				fmt.Println(err.Error())
			}
		}
	}

	fmt.Printf("Found %d passwords\n", counter)
	fmt.Printf("Imported %d passwords\n", counter-errcounter)
}

func insert(item item, cmd *exec.Cmd) error {
	b := strings.Builder{}
	writeln := func(name string, line string) {
		if len(line) == 0 {
			return
		}
		line = filter(line)
		if len(name) > 0 {
			b.WriteString(name + ": ")
		}
		b.WriteString(line)
		b.WriteRune('\n')
	}
	if *dry {
		writeln("", "***PASS***")
	} else {
		writeln("", item.password)
	}
	writeln("login", item.username)
	writeln("url", item.url)
	writeln("notes", item.notes)
	if len(item.sections) > 0 {
		for _, section := range item.sections {
			writeln("", section)
		}
	}

	var out bytes.Buffer
	var errout bytes.Buffer
	cmd.Stdin = strings.NewReader(b.String())
	cmd.Stdout = &out
	cmd.Stderr = &errout
	err := cmd.Run()
	if err != nil {
		errs = append(errs, errors.New(fmt.Sprintf("Error for password %q:\n%s", item.title, errout.String())))
		return errors.New(fmt.Sprintf("Error for password %q:\n%s", item.title, errout.String()))
	}
	if *dry {
		fmt.Printf("Catting %s\n%s\n", item.title, out.String())
	}
	return nil
}

func filter(line string) string {
	for old, new := range filters {
		line = strings.ReplaceAll(line, old, new)
	}
	return line
}

func readFile(recordChan chan []string) {
	defer close(recordChan)
	f, err := os.Open(*file)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	r := csv.NewReader(bufio.NewReader(f))
	r.FieldsPerRecord = -1
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		recordChan <- record
	}
}
