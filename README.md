# !WARNING! - This is _plain text_ operation
_make sure you know what you are doing._

# CSV2PASS

Import csv formatted data to [pass](https://www.passwordstore.org) store.
 - Usage
 - CSV Format
 - 1Password export to csv

## Usage
You can use cli interface or edit the source code.

**cli interface**
```
Usage of csv2pass:
  -dry
        dry run, just cat everything out with hidden password
  -file string
        Import data file
  -meta
        write additional data to separate lines after password (default true)
```
**or inside code**
```
file    = flag.String("file", "", "Import data file")
meta    = flag.Bool("meta", true, "write additional data to separate lines after password")
dry     = flag.Bool("dry", false, "dry run, just cat everything out with hidden password")
filters = map[string]string{} // will replace every occurance of <key> to <value> on every line
```

## CSV Format
`<title>,<password>,<login>,<url>,<notes>,<...sections>`

- No headers
- Line can have different number of columns/parts


**Content of password file**

`title` is used as name of pass

rest is optional and will not be added if empty. But csv needs to have those (5) fields at least there, just leave them empty.
```
<password>
login: <login>
url: <url>
notes: <notes>
<sections>
```

## 1Password export to csv

**Tools used**  
 - `op` cli tool from 1password,
 - `jq` json parsing tool

 1. Export all items to `allitems.json`.
```
op list items | op get item - > allitems.json
```
 2. Parse json to `allitems.txt`
```
cat allitems.json | jq "[(.overview.title, (.details.fields[] | select(.designation == \"password\").value)?, (.details.fields[] | select(.designation == \"username\").value)?, .overview.URLs[0].u, .details.notesPlain, (.details.sections[].fields[]|(.t+\": \"+.v)))?]" > allitems.txt
```
 example output: 
 ```
[
  "my.title",
  "123456789abcd",
  "",
  null,
  ""
]
[
  "my.title.2",
  "987654321bcda",
  "my@super.mail",
  "https://www.example.com",
  ""
  "section:value"
]
 ```
 3. Magic inside your editor to make it to the final `allitems.csv` (_in vim is pretty easy to do_)

My approach:
 - Select block; `J` to make it one-line; remove brackets
 - Remove empty spaces (`s/\s\+//g`)  
 - Substitute `null` to `""` (`s/null/""/g`)  
 - _macro this and repeat_

example output:
 ```
"my.title","123456789abcd","","",""
"my.title.2","987654321bcda","my@super.mail","https://www.example.com",""
 ```

## TODO (help)

 - [ ] cleanup code & comment
 - [ ] add tests
 - [ ] require only first column of csv. (now it needs to have them there)
 - [ ] edit `jq` parsin command to not include `null` but rather `""`
 - [ ] ? maybe write simple script to parse `allitems.json` to final `allitems.csv` (and do not force user to parse them)
 - [ ] make `force` work (for me it just overwrite even without `-f`)
 - [ ] better output
   - [ ] correctly handle errors (maybe pause and prompt for user action)
